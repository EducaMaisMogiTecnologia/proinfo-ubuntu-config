#!/bin/bash

install -m 644 systemd/xorg-daemon.s* /etc/systemd/system
install -m 644 ../comum/udev/* /etc/udev/rules.d
install -m 644 udev/* /etc/udev/rules.d

install -d /etc/X11/xorg.conf.d
install -m 644 xorg/9*.conf /etc/X11/xorg.conf.d
install -m 755 ../comum/scripts/update-xorg-conf /usr/local/bin
install -m 755 scripts/seat-attach-helper /usr/local/bin
install -m 755 scripts/xorg-daemon /usr/local/bin

install -d /etc/xdg/lightdm/lightdm.conf.d
install -m 644 ../comum/lightdm/9*.conf /etc/xdg/lightdm/lightdm.conf.d
install -m 644 lightdm/93-autologin.conf /etc/xdg/lightdm/lightdm.conf.d

update-xorg-conf "Silicon.Motion" /etc/X11/xorg.conf.d/98-proinfo-*.conf
systemctl enable xorg-daemon.socket
systemctl start xorg-daemon.socket

apt-add-repository -y ppa:ubuntu-multiseat/xf86-video-nested
apt update
apt -y upgrade
apt -y install curl xserver-xorg-video-{siliconmotion,nested}-hwe-16.04 compton numlockx
(cd ../comum ; ./configurar-compton.sh ; ./configurar-adobe-flash.sh ; ./configurar-userful-rescue.sh)

udevadm trigger
systemctl restart lightdm
