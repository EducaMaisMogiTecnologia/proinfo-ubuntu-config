#!/bin/bash

apt update
apt -y install libpam-mount bindfs python-gnomekeyring

mkdir -pm 0777 /var/freeze-data/{documents,pictures,music,videos}
install -d /etc/xdg/lightdm/lightdm.conf.d
install -m 644 ../comum/lightdm/80-disable-guest.conf /etc/xdg/lightdm/lightdm.conf.d
install -m 755 ../comum/scripts/create-freeze-users /usr/local/sbin
install -m 755 ../comum/scripts/mount-wrapper /usr/local/sbin
install -m 644 ../comum/pam/pam_mount.conf.xml /etc/security
install -m 644 ../comum/xubuntu/*.policy /usr/share/polkit-1/actions
install -m 755 ../comum/scripts/freeze-session-auto /usr/local/bin

/usr/local/sbin/create-freeze-users 2
