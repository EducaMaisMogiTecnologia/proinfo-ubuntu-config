#!/bin/bash

install -m 644 ../comum/udev/* /etc/udev/rules.d
install -m 644 udev/* /etc/udev/rules.d

install -d /etc/X11/xorg.conf.d
install -m 644 xorg/* /etc/X11/xorg.conf.d
install -m 755 ../comum/scripts/update-xorg-conf /usr/local/bin
install -m 755 scripts/seat-attach-helper /usr/local/bin

install -d /etc/xdg/lightdm/lightdm.conf.d
install -m 644 ../comum/lightdm/9*.conf /etc/xdg/lightdm/lightdm.conf.d
install -m 644 lightdm/*.conf /etc/xdg/lightdm/lightdm.conf.d

update-xorg-conf "Silicon.Motion" /etc/X11/xorg.conf.d/99-sm501.conf

apt update
apt -y upgrade
apt -y install curl xserver-xorg-video-siliconmotion-hwe-16.04 compton numlockx
(cd ../comum ; ./configurar-compton.sh ; ./configurar-adobe-flash.sh ; ./configurar-userful-rescue.sh)

udevadm trigger
systemctl restart lightdm
