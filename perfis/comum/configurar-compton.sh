#!/bin/bash
install -m 644 compton/compton.desktop /etc/xdg/autostart
install -m 644 compton/compton.conf /etc/xdg

if [ -x /usr/bin/xfconf-query ]
then
  install -m 755 scripts/compton-wrapper /usr/local/bin
  sed -i -e 's/%COMPTON%/compton-wrapper/' /etc/xdg/autostart/compton.desktop
else
  sed -i -e 's/%COMPTON%/compton/' /etc/xdg/autostart/compton.desktop
fi
