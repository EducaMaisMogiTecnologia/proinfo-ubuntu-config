#!/bin/sh
echo '#192.18.x.x server' >> /etc/hosts
echo '#192.18.x.x apt-cacher-ng-server' >> /etc/hosts
echo '#192.18.x.x nfs-share-server' >> /etc/hosts
echo '#nfs-share-server:/alunos /var/freeze-data nfs4 noauto,x-systemd.automount,x-systemd.device-timeout=10,timeo=14,x-systemd.idle-timeout=1min 0 0' >> /etc/fstab
echo '#Acquire::http::Proxy "http://apt-cacher-ng-server:3142";' > /etc/apt/apt.conf.d/00proxy
