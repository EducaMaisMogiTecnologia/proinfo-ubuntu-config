#!/bin/bash

install -m 644 systemd/userful-rescue-* /etc/systemd/system
install -m 755 scripts/userful-rescue-{enable,disable} /usr/local/bin
install -m 755 grub/42_userful-rescue /etc/grub.d
mkdir -p /boot/userful-rescue
install -m 644 grub/userful-rescue-live-20160628-i386.iso /boot/userful-rescue
update-grub

cat << EOF

[AVISO] Para ativar a solução para o bug da tela listrada, execute:

            sudo userful-rescue-enable

        Uma vez concluído o processo, você pode reiniciar o computador
        sempre que necessário.

        A qualquer momento, você pode desativar a solução executando:

            sudo userful-rescue-disable

EOF
